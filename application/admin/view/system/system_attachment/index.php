{extend name="public/container"}
{block name="head"}
<style>
.panel-body{min-height: 800px;}
.panel-body .image-item{
    position: relative;
    display: inline-block;
    width: 120px;
    height: 120px;
    border: 1px solid #CCC;
    cursor: default;
    margin: 5px 0 0 5px;}
.panel-body .image-item .image-close {
    position: absolute;
    right: 0;
    background: url('/public/system/plug/umeditor/dialogs/image/images/close.png');
    width: 17px;
    height: 17px;
    cursor: pointer;
    z-index: 1;
}
.panel-body .image-item img{width: 100px;
    height: 100px;
    margin: 9px;}
</style>
{/block}
{block name="content"}
<div class="row m-b-lg">
    <div class="col-sm-12">
    <div class="tabs-container">
        <div class="tabs-left">
            <?php

            $typelist = array();
            foreach ($typearray as $key=>$value){
                $typelist[$key]['key'] = $key;
                $typelist[$key]['value'] = $value;
            }
            ?>
            <ul class="nav nav-tabs">
                {volist name="typelist" id="vo" key="k"}
                {if condition="$vo.key eq $pid"}
                <li class="active"><a href="{:Url('index',array('pid'=>$vo.key))}">{$vo.value}</a></li>
                {else/}
                <li><a href="{:Url('index',array('pid'=>$vo.key))}">{$vo.value}</a></li>
                {/if}
                {/volist}
            </ul>
            <div class="tab-content ">
                <div id="tab" class="tab-pane active">
                    <div class="panel-body">
                        {volist name="list" id="vo"}
                        <div class="image-item">
                            <div class="image-close" data-url="{:Url('delete',array('att_id'=>$vo.att_id))}"></div>
                            <img src="{$vo.att_dir|ltrim='.'}"/>
                        </div>
                        {/volist}
                        {include file="public/inner_page"}
                    </div>
                </div>


            </div>

        </div>
    </div>
    </div>

</div>
{/block}
{block name="script"}
<script>
    $('.image-close').on('click',function(){
        window.t = $(this);
        var _this = $(this),url =_this.data('url');
        $eb.$swal('delete',function(){
            $eb.axios.get(url).then(function(res){
                console.log(res);
                if(res.status == 200 && res.data.code == 200) {
                    $eb.$swal('success',res.data.msg);
                    _this.parent().remove();
                }else
                    return Promise.reject(res.data.msg || '删除失败')
            }).catch(function(err){
                $eb.$swal('error',err);
            });
        })
    });

</script>
{/block}
